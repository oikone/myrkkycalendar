import re
import operator


# Custom imports:
import myrkky as my
import constants as const


class MCParser:
    def __init__(self):
        """ Handles parsing a MyCourses Calendar export event
            
            Tries to analyse the summary and break it into logical parts
            The main focus is on the SUMMARY field of the ical export:
            A lot of information is in that. Currently the following:
            
            Data type       | Example
            ----------------|---------------------------
            Course detail   | Course Code, Course name
            Event type      | Lecture / Exercise / Quiz
            Course timespan | 09.04.2018-25.05.2018
            Address         | Otakaari 1
            Building        | TUAS
            Room            | Y201a
            
            """
        # Hierarchy to solve uncertain cases using weighted sum:
        self.hierarchy = []
        
        # DIfferent types of data parseable:
        self.myrkkyTypes = [
                            my.McAddress(),
                            my.McCourseDetail(),
                            my.McDate(),
                            my.McMultilangInfo(),
                            my.McRoom(),
                            ]
    
    def parse_summary(self, summary, event=my.MyrkkyEvent()):
        """ Parse information from the summary to its appropriate fields
            
            Arguments:
            summary (str): Summary as string
            event (MyrkkyEvent): an event object to update. New one
            is created if not provided
            
            Return (MyrkkyEvent):
            A parsed event containing the info from summary
            """
        # Split into substrings based on separators
        parts = [p.strip() for p in re.split(r'[,\/]+ ', summary)]
        
        # Try to assign a label for every substring (NER)
        selected = []
        for i, p in enumerate(parts):
            labels = {}
            for mt in self.myrkkyTypes:
                labels[mt.tag] = mt.evaluate(p)
        
            # Select the one with highest confidence:
            tag = max(labels.items(), key=operator.itemgetter(1))[0]
            
            # Go through some special cases:
            if i == 0 and tag != const.ML_INFO_TAG:
                tag = const.SUMMARY_TAG
    
    elif i == 1 and not sum(labels.values()):
        tag = const.DESCRIPTION_TAG
            
            elif list(labels.values()).count(1.0) > 1:
                tag = const.ALT_DESCRIPTION_TAG
            
            elif list(labels.values()).count(1.0) == 0:
                if selected and selected[-1] in [const.CLASSROOM_TAG,
                                                 const.ADDRESS_TAG]:
                    tag = const.LOCATION_TAG
                        
                                                 else:
                    tag = const.UNKNOWN_TAG
            selected.append(tag)
        
        # If confident enough, return a Myrkky object with information
        return my.MyrkkyEvent(**dict(zip(selected, parts)))
    
    def check_timestamps(self, dt_start, dt_end, event):
        """ Check how sensible the timestamps are
            
            i.e. should the event rather be a full day event
            
            TODO: Somehow make use this sort of syntax:
            DTSTART;VALUE=DATE:20181002
            DTEND;VALUE=DATE:20181003
            """
        if dt_start == dt_end:
            pass
        raise NotImplementedError()
