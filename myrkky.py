import re
import constants as const


class MyrkkyCourse:
    def __init__(self, course_code, timespan):
        """ Container for a MyCourses course
            
            Identifiable with the course code
            Contains events and timespan
            
            """
        self.course_code = course_code
        self.timespan = timespan


class MyrkkyEvent:
    def __init__(self, *args, **kwargs):
        """ Container for Study Event
            
            Contains important information of the study event
            Used to pass information explicitly around the
            parser and enhancer workflow.
            
            Initiation arguments:
            **kwargs: Data for class properties (details)
            
            """
        # Event details:
        self.event_type = kwargs.get(const.EVENT_TYPE_TAG, None)
        self.event_code = kwargs.get(const.EVENT_CODE_TAG, None)
        self.summary = kwargs.get(const.SUMMARY_TAG, None)
        self.description = kwargs.get(const.DESCRIPTION_TAG, None)
        self.alt_description = kwargs.get(const.ALT_DESCRIPTION_TAG, None)
        self.multilingual_info = kwargs.get(const.ML_INFO_TAG, None)
        # Location details:
        self.geo = kwargs.get(const.LOCATION_TAG, None)
        self.country = kwargs.get(const.COUNTRY_TAG, None)
        self.postcode = kwargs.get(const.POSTCODE_TAG, None)
        self.city = kwargs.get(const.CITY_TAG, None)
        self.location = kwargs.get(const.LOCATION_TAG, None)
        self.address = kwargs.get(const.ADDRESS_TAG, None)
        self.classroom = kwargs.get(const.CLASSROOM_TAG, None)
        self.building = kwargs.get(const.BUILDING_TAG, None)
        # Course details:
        self.course_name = kwargs.get(const.COURSE_NAME_TAG, None)
        self.course_code = kwargs.get(const.COURSE_CODE_TAG, None)
        self.course_webpage = kwargs.get(const.COURSE_WEBPAGE_TAG, None)
        self.course_contact = kwargs.get(const.COURSE_CONTACT_TAG, None)
        self.course_detail = kwargs.get(const.COURSE_DETAIL_TAG, None)
        # Others:
        self.timespan = kwargs.get(const.COURSE_TIMESPAN_TAG, None)
        # Supportive:
        self.errors = None
        self.unknown = kwargs.get(const.UNKNOWN_TAG, None)
    
    def form_ical_fields(self):
        """ Combines fields nicely to support icalendar format
            
            Arguments:
            None
            
            Return (dict):
            A dictionary with keys as icalendar supported strings
            and values as strings.
            
            Aims to form a nice, consistent and short summary:
            Lecture, AS1, Stochastics
            
            Practically just combines the fields in a nice way.
            """
        ical_event = {
            'summary': self._form_ical_summary(),
            'description': self._form_ical_description(),
            'url': self.course_webpage,
            'location': self._form_ical_address(),
        #'geo': self.location
        }
        
        
        return ical_event
    
    def _form_ical_summary(self):
        summary = f'{self.event_type}, {self.course_name}, {self.classroom}, {self.building}'
        return summary.replace('None', '')
    
    def _form_ical_description(self):
        rstring = f'{self.description}\n\n'
        if self.course_webpage is not None:
            rstring += f'Course website: {self.course_webpage}\n\n'
        if self.summary is not None:
            rstring += f'Summary:\n {self.summary}\n\n'
        if self.unknown is not None:
            rstring += f'{self.unknown}\n\n'
        if self.timespan is not None:
            rstring += f'Course schedule: {self.timespan}'
        
        return rstring.replace('None', '')
    
    def _form_ical_address(self):
        rstring = f'{self.address}, {self.postcode} {self.city}, {self.country}'
        return rstring.replace('None', '')
    
    def __repr__(self):
        attrs = vars(self)
        l_max = max(len(a) for a in attrs.keys())
        
        rstring = '<MyrkkyEvent:> (\n'
        for tag, val in attrs.items():
            rstring += tag.rjust(l_max)+': '+str(val)+'\n'
        rstring += ')'
        return rstring


class McData:
    def __init__(self):
        """ An abstract class for the MyCourses calendar data
            
            Requires following properties to be defined for every subclass:
            self.patterns = []
            self.tag = ''
            
            p is a collection of regex patterns which are searched from the
            string. The maximum coverage of the best pattern match is returned.
            
            Methods are optional to implement. For example, evaluate() can
            be overridden to use a more sophisticated method for evaluation.
            
            """
        self.patterns = None
        self.tag = None
    
    def evaluate(self, s):
        """ Estimate the probability of s being a certain data type
            
            Arguments:
            s (str): A string to evaluate
            
            Return (float):
            A value in range [0, 1], showing the confidence.
            """
        if self.patterns is None:
            raise NotImplementedError('McData pattern not defined')
        
        best_m = 0
        for p in self.patterns:
            m = re.findall(p, s)
            if m and len(m[0]) > best_m:
                best_m = len(m[0])

    return 0 if not m else best_m / len(s)

def __str__(self):
    if self.tag is None:
        raise NotImplementedError('McData str method not implemented')
    
    def __repr__(self):
        return str(self)


class McDate(McData):
    def __init__(self):
        self.patterns = [
                         re.compile(r'\d{2}\.\d{2}\.\d{4}-\d{2}\.\d{2}\.\d{4}', re.UNICODE)
                         ]
        self.tag = const.COURSE_TIMESPAN_TAG


class McCourseDetail(McData):
    def __init__(self):
        self.patterns = [
                         re.compile(r'[\w]{2,4}-[\w]{4,} - .+', re.UNICODE)
                         ]
        self.tag = const.COURSE_DETAIL_TAG


class McAddress(McData):
    def __init__(self):
        self.patterns = [
                         re.compile(r'\w{4,} \d+', re.UNICODE)
                         ]
        self.tag =  const.ADDRESS_TAG


class McRoom(McData):
    def __init__(self):
        self.patterns = [
                         r'[\w]+\s+\([\w\-]+\)',
                         r'[a-zA-Z]?\d{2,4}\w?'
                         ]
        self.tag = const.CLASSROOM_TAG


class McMultilangInfo(McData):
    def __init__(self):
        self.patterns = [
                         re.compile(r'.+ \w+\/\w+\/\w+', re.UNICODE)
                         ]
        self.tag = const.ML_INFO_TAG
    
    def __str__(self):
        return const.ML_INFO_TAG

