""" Some constants to keep the shit together.
    
    Mainly based on MyrkkyEvent needs:
    self.event_type = None
    self.summary = None
    self.description = None
    self.alt_description = None
    
    # Location details:
    self.location = None
    self.address = None
    self.classroom = None
    self.building = None
    
    # Course details:
    self.course_name = None
    self.course_code = None
    self.course_webpage = None
    self.course_contact = None
    
    Also allows us to nicely bridge the line to icalendar updates!
    TODO: Mark values which are supported by icalendar
    """

# Event details:
EVENT_TYPE_TAG = 'event type'
EVENT_CODE_TAG = 'event code'
SUMMARY_TAG = 'summary'
DESCRIPTION_TAG = 'description'
ALT_DESCRIPTION_TAG = 'alt description'

# Location details:
COUNTRY_TAG = 'country'
POSTCODE_TAG = 'postcode'
CITY_TAG = 'state'
LOCATION_TAG = 'location'
ADDRESS_TAG = 'address'
CLASSROOM_TAG = 'classroom'
BUILDING_TAG = 'building'

# Course details:
COURSE_NAME_TAG = 'course name'
COURSE_CODE_TAG = 'course code'
COURSE_WEBPAGE_TAG = 'course webpage'
COURSE_CONTACT_TAG = 'course contact'
COURSE_DETAIL_TAG = 'course detail'
COURSE_TIMESPAN_TAG = 'course timepan'

# Other
DATE_TAG = 'date'
ML_INFO_TAG ='multilingual info'
UNKNOWN_TAG = 'unknown'


# Language indices in the  multilingual info:
LANG = {
    'FI': 0,
    'EN': 1,
    'SE': 2,
}

SELECTED_LANGUAGE = 'EN'
LANG_IDX = LANG[SELECTED_LANGUAGE]

# Address - building lookups:
ADDR_BUILDING_LOOKUP = {
    'Maarintie 8': 'TUAS',
    'Otakaari 1': 'Main Building',
    'Konemiehentie 2': 'T-Building'
}
BUILDING_ADDR_LOOKUP = dict((reversed(item) for item in ADDR_BUILDING_LOOKUP.items()))

CRAWL_SAVEFILE = 'crawled_courses_dump.json'

COUNTRY_ABBR = {
    'fi': 'Finland',
    'se': 'Sweden',
    'de': 'Germany',
    'no': 'Norway',
}
