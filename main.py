""" Parser for MyCourses calendar exports.
    
    The calendar syntax is abysmal: the description and summary
    contain excess information and usually in the wrong fields.
    The information can be enhanced with appropriate links, geo,
    times, calendar /event types and reminders.
    
    Project for less overhead and "Where in the hell and what
    time was that class again."
    
    Created 18.9.2018 (c) Olli Riikonen
    """


# Standard python imports:
from os import path
import logging

# PiPy imports:
import icalendar as ical

# Custom imports:
import McParser as mc
import McEnhancer as me

def load_ical_data(ical_filename):
    """ Load ical data from a file
        
        Arguments:
        ical_filename (str): Name of the file containing ical data
        
        Return (ical):
        An ical object with the ical data
        """
    with open(ical_filename, 'r') as infile:
        datas = infile.read()
    return ical.Calendar.from_ical(datas)


def save_ical_data(ical_data, ical_filename):
    """ Save ical data to a file
        
        Arguments:
        ical_data (ical): ical data to save
        ical_filename (str): Name of the save file
        
        Return (None):
        Nothing
        """
    outputFileName = path.join("edited_"+ical_filename)
    with open(outputFileName, 'wb') as outputFile:
        outputFile.write(ical_data.to_ical())


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)
    # -------------- LOADING --------------
    # Load the ical file:
    ICAL_FILE = "icalexport.ics"
    ical_data = load_ical_data(ICAL_FILE)
    
    # -------------- PARSING & ENHANCING --------------
    # Init the parser:
    myParser = mc.MCParser()
    myEnhancer = me.MCEnhancer()
    
    # Go through the events and parse the information:
    for i, event in enumerate(ical_data.walk('vevent')):
        
        # Print samples of the results:
        if i > -1:
            logger.debug(f'ORIGINAL:\n    {event["summary"]}')
            
            # Parse the data
            res = myParser.parse_summary(event['summary'])
            logger.debug(f'PARSED:\n    {res}\n')
            
            # Enhance the data:
            myEnhancer.enhance(res)
            logger.debug(f'ENHANCED:\n    {res}\n')
            
            # Update the fields:
            event.update(res.form_ical_fields())


# SAVE
save_ical_data(ical_data, 'icalexport.ics')
myEnhancer.save_courses()
