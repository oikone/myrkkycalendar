# Standard imports:
import requests

# PyPi imports:
from geopy.geocoders import Nominatim
from bs4 import BeautifulSoup
import json

# Custom imports:
import constants as const


class MCEnhancer:
    
    def __init__(self):
        
        """ Handles adding extra information to data
            
            For example websites, GPS locations, recommendations etc.
            
            TODO: Cache courses, classromos and buildings to reduce need for
            unnecessary crawling
            """
        self.geolocator = Nominatim()
        self.enhance_methods = [
                                self._parse_course_details,
                                self._parse_multilingual_info,
                                self._parse_location,
                                self._get_location,
                                self._get_website,
                                self._last_resort,
                                ]
            
                                # Caches:
                                self.courses = {
                                
                                }
                                
                                # Load previously crawled:
        self.__load_courses()
    
    
    def enhance(self, myrkky):
        """ Edit MyrkkyEvent to include more accurate information
            
            Arguments:
            myrkky (MyrkkyEvent): A calendar event to enhance
            
            Returns (None):
            Nothing
            
            TODO: Make to return all enhanced fields?
            """
        for em in self.enhance_methods:
            em(myrkky)

def _last_resort(self, myrkky):
    """ Check some last minute tricks
        
        Depebds on the facts that this will be the last resort
        some types that might be found with this:
        Quiz open
        Quiz closes
        Feedback opens
        Feedback closes
        """
            if myrkky.event_type is not None:
            return
                
                leftovers = ''
                    
                    if myrkky.summary is not None:
                        leftovers = myrkky.summary.lower()
                            
                            if myrkky.unknown is not None:
                                leftovers += myrkky.unknown.lower()
                                    
                                    if not leftovers:
                                        return
                                            
                                            if 'feedback' in leftovers:
                                                event_type = 'Feedback'
                                                    elif 'quiz' in leftovers:
                                                        event_type = 'Quiz'
                                                            elif 'questi' in leftovers:
                                                                event_type = 'Questionnaire'
                                                                    elif 'dl' in leftovers:
                                                                        event_type = 'Dead Line'
                                                                            elif 'homework' in leftovers:
                                                                                event_type = 'Homework'
                                                                                    elif 'lab' in leftovers:
                                                                                        event_type = 'Lab'
                                                                                            elif 'assignm' in leftovers:
                                                                                                event_type = 'Assignment'
                                                                                                    elif 'tentt' in leftovers or 'exam' in leftovers:
                                                                                                        event_type = 'Exam'
                                                                                                            else:
                                                                                                                event_type = 'Portal to hell'
                                                                                                                    
                                                                                                                    if 'close' in leftovers:
                                                                                                                        tail = ' closes'
                                                                                                                            elif 'open' in leftovers:
                                                                                                                                tail = ' opens'
                                                                                                                                    elif 'due' in leftovers:
                                                                                                                                        tail = ' is due'
                                                                                                                                            else:
                                                                                                                                                tail = ''
                                                                                                                                                    
                                                                                                                                                    myrkky.event_type = event_type+tail

def _parse_course_details(self, myrkky):
    """ Parse course name and code from course detail info
        """
            if myrkky.course_detail is None:
            return
                
                _code_name = myrkky.course_detail.split(' - ')
                myrkky.course_code = _code_name[0] if len(_code_name) > 1 else None
                myrkky.course_name = _code_name[1] if len(_code_name) > 1 else None

def _parse_multilingual_info(self, myrkky):
    """ Parse Event type from the general multilingual info
        """
            if myrkky.multilingual_info is None:
            return
                
                info_code, infos = myrkky.multilingual_info.split(' ')
                infos = infos.split('/')
                myrkky.event_code = info_code
                    myrkky.event_type = infos[const.LANG[const.SELECTED_LANGUAGE]]

def _parse_location(self, myrkky):
    """ Parse building / address using a look-up table
        """
            if myrkky.building is None:
            myrkky.building = const.ADDR_BUILDING_LOOKUP.get(myrkky.address, None)
                if myrkky.address is None:
                    myrkky.address = const.BUILDING_ADDR_LOOKUP.get(myrkky.building, None)


def _get_location(self, myrkky):
    """ Returns a string containing the longitude and latitude
        
        Arguments:
        address (str): a string containing the address
        for which to search the location
        Return (str):
        A string of lat;long
        """
            address = myrkky.address
                if address is None:
                    return
                        
                        if self.__check_saved(const.LOCATION_TAG, myrkky.course_code):
                            location = self.courses[myrkky.course_code][const.LOCATION_TAG]
                                else:
                                    location = self.geolocator.geocode(address, addressdetails=True).raw
                                        
                                        if not location:
                                            return
                                                
                                                # Extract information:
                                                _country_code = location['address'].get('country_code', None)
                                                _postcode = location['address'].get('postcode', None)
                                                _city = location['address'].get('city', None)
                                                _geo = ';'.join([str(location['lat']),
                                                                 str(location['lon'])])
                                                    
                                                                 # Set information:
                                                                 myrkky.location = _geo
                                                                     myrkky.country = const.COUNTRY_ABBR.get(_country_code, _country_code)
                                                                     myrkky.postcode = _postcode
                                                                         myrkky.city = _city
                                                                             
                                                                             self.__save_checked(const.LOCATION_TAG, myrkky.course_code, location)



def _get_website(self, myrkky):
    """ Returns a string containing the URL address
        
        Uses the first search result from Google for the search term
        given in parameter key
        
        Arguments:
        key: a string containing the search term for which
        the "I'm feeling lucky" search is performed
        Return (str):
        The url for the course website (not 100% accurate!)
        
        NOTE: Needs heavy revising!
        """
            course_name = myrkky.course_name if myrkky.course_name is not None else ''
                course_code = myrkky.course_code if myrkky.course_code is not None else ''
                    year = '2018'
                        
                        if not (course_code or course_name):
                            return
                                
                                if self.__check_saved(const.COURSE_WEBPAGE_TAG, course_code):
                                    myrkky.course_webpage = self.courses[course_code][const.COURSE_WEBPAGE_TAG]
                                    return
                                        
                                        key = course_name+'+'+course_code+'+'+year
                                            
                                            max_tries = 5
                                                
                                                for tries in range(max_tries):
                                                    goog_search = "https://www.google.fi/search?rls=en&q=" + \
                                                        key + "&ie=UTF-8&oe=UTF-8"
                                                            r = requests.get(goog_search)
                                                            soup = BeautifulSoup(r.text, "html.parser")
                                                            if "mycourses" in soup.find('cite').text:
                                                                _webpage = soup.find('cite').text
                                                                break
                                                                    else:
                                                                        _webpage = "https://bit.ly/myrkky"
                                                                            
                                                                            myrkky.course_webpage = _webpage
                                                                                self.__save_checked(const.COURSE_WEBPAGE_TAG, course_code, _webpage)


def _get_recommendations(self):
    # TODO: Implememt
    raise NotImplementedError()
    
    def get_exam(self):
        # TODO: Implememt
        raise NotImplementedError()
    
    def __check_saved(self, method, course_code):
        """ Check if the method has saved info for the course
            
            Allows us to skip the same location, webpage etc crawling.
            
            Arguments:
            method (str): Method name to look up
            course_code (str): Course code to check
            
            Return (bool):
            Whether value was found or not
            
            
            """
        if course_code is None or course_code not in self.courses:
            return False
        return method in self.courses[course_code]
    
    
    def __save_checked(self, method, course_code, value):
        """ Save value to cache
            
            Arguments:
            method (str): Method to look up
            course_code (str): course code to look up
            value (object): Value to save
            
            Return (None):
            Nothing
            """
        if course_code not in self.courses:
            self.courses[course_code] = {}
        self.courses[course_code][method] = value
    
    def save_courses(self):
        """ Save the crawled infromation as json
            
            (To prevent excess crawls)
            """
        with open(const.CRAWL_SAVEFILE, 'w') as fp:
            json.dump(self.courses, fp)
    
    def __load_courses(self):
        """ Load the previously crawled information
            
            (To prevent excess crawls)
            
            TODO: Remove try-except with something better
            """
        try:
            with open(const.CRAWL_SAVEFILE, 'r') as fp:
                self.courses = json.load(fp)
        except FileNotFoundError:
            print('Crawl file not found. Creating one now...')
            self.courses = {}
            self.save_courses()
