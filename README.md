# MyrkkyCalendar

A simple parser for the horrible syntax given by MyCourses calendar exports.

**NOTE**
> Moving the project from repl.it to git has destroyed code formatting, resulting in some very funky lines and seemingly horrible nests.PLease be careful. Waits un urgent hot-fix.

## Motivation
The calendar exports in MyCourses are horrible. The summary field of the exported [iCalendar](https://tools.ietf.org/html/rfc5545) file contains far too much information. For example, this is the summary field of an exercise lesson for one course:
```H01 Harjoitukset/Exercises/Övningar, AS5 (1621), TUAS, Maarintie 8 / ELEC-E8102 - Distributed and Intelligent Automation Systems P, 13.09.2018-10.12.2018```

![alt text](img/orig.png)
Figure 1: An example events and detail of one exported MyCourses calendar


## Logic
The program mainly focuses on parsing the information from the overbloated `SUMMARY` field. The parsed information is then enhanced with selected methods and finally combined into a icalendar supported format which in turn can be exported.

### Parsing
It first splits the field and tries to assign each part a tag describing what it information it depicts. These informations usually are some combination fo the following:

- Timespan of the course
- Course name and course code
- Event type
- Event location


### Enhancing
The system also provides a class to enhance the information in the event. The enhancements currently include adding the geolocation, more detailed address and website for the course. More enhancements can be added

### Creation
Finally, the system implements a simple collector for combining all the parsed and enhanced information. The system produces what the developer deemed as most efficient combination of the information. The factor affecting the choices include the aesthetics, readability, most used information and especially scaling on a mobile screen for maximum information transferring with minimum interaction.

The original SUMMARY field is left to the DESCRIPTION field of the event


## Example

![alt text](img/parsed.png)
Figure 2: An example events and detail of parsed MyCourses calendar

Here we see an output of the script, printing the parsed information, as well as the enhanced information with more detailed location and URL information added:
```
DEBUG:__main__:PARSED:
<MyrkkyEvent:> (
event_type: None
event_code: None
summary: None
description: None
alt_description: None
multilingual_info: H01 Harjoitukset/Exercises/Övningar
location: TUAS
address: Maarintie 8
classroom: AS5 (1621)
building: None
course_name: None
course_code: None
course_webpage: None
course_contact: None
course_detail: ELEC-E8102 - Distributed and Intelligent Automation Systems P
timespan: 13.09.2018-10.12.2018
errors: None
unknown: None
)

DEBUG:__main__:ENHANCED:
<MyrkkyEvent:> (
event_type: Exercises
event_code: H01
summary: None
description: None
alt_description: None
multilingual_info: H01 Harjoitukset/Exercises/Övningar
location: TUAS
address: Maarintie 8
classroom: AS5 (1621)
building: TUAS
course_name: Distributed and Intelligent Automation Systems P
course_code: ELEC-E8102
course_webpage: https://mycourses.aalto.fi/course/view.php?id=20981
course_contact: None
course_detail: ELEC-E8102 - Distributed and Intelligent Automation Systems P
timespan: 13.09.2018-10.12.2018
errors: None
unknown: None
```

## Future ideas
Like any other project, this one as well has many improvements thought out. Here are listed a few:

- A collective database to avoid excessive crawling
- Links to relative files (would require MyCourses crawling project)
- Automatic filtering of overlapping events
    - Allows easier planning
    - Would be executed by prioritising some events over other (e.g. Lectures over Exercises)
    - Would require implementation of a course class or similar
- More precise address creation
- Added Google Calendar layer to automatically add events to Google calendar
    - Would allow colouring of the events (e.g. different colours for Exercises/Lectures etc.)
- Add ical validation
- Add progressbar to see progress
- Use decorators
- Make sure code follows PEP8

## Testing with your own data
If you want to test the system with your own data, simply replace the `icalexport.ics` with your personal MyCourses export and run the script. It updates the `edited_icalexport.ics` with the parsed information.

**NOTE**
> Please use python3.6 or later. The system utilises functional string introduced in python3.6

**NOTE**
>The Google Calendar does not support the icalendar's GEO field. If such field is present in any of the imported events, the import fails.

**NOTE**
> I have no possibility to update the `requirements.txt` so some imports have to be installed manually by trial-and-error


